function Error({ statusCode, res, err }) {
    return (
        <>
            <p>
                {statusCode
                    ? `An error ${statusCode} occurred on server`
                    : 'An error occurred on client'}
            </p>
            <p>
                {JSON.stringify(res)}
            </p>
            <p>
                {JSON.stringify(err)}
            </p>
        </>
    )
}

Error.getInitialProps = ({ res, err }) => {
    const statusCode = res ? res.statusCode : err ? err.statusCode : 404
    return { 
        statusCode:statusCode, 
        res:res, 
        err:err
    }
}

export default Error