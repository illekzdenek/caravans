import type { NextApiRequest, NextApiResponse } from "next";
import { AVAILABLE, MAX_PRICE, MIN_PRICE, TYPE } from "../../constants/config";

import data from "../../src/__nemazat/data.json";

export default function handler(req: NextApiRequest, res: NextApiResponse) {

  const getRandomInt = (max: number) => {
    return Math.floor(Math.random() * max);
  };

  const filterData = getFilterData(req.body.minPrice,req.body.maxPrice,req.body.type,req.body.availability,req.body.count)

  const newData = {
    count: filterData.length,
    items: filterData.map(
      ({
        location,
        instantBookable,
        name,
        passengersCapacity,
        pictures,
        sleepCapacity,
        price,
        toilet,
        shower,
        vehicleType,
      }) => {
        return {
          location,
          instantBookable,
          name,
          passengersCapacity,
          sleepCapacity,
          price,
          vehicleType,
          toilet,
          shower,
          // toilet: [true, false][getRandomInt(2)],
          // shower: [true, false][getRandomInt(2)],
          // vehicleType: ['Campervan', 'Intergrated', 'Alcove', 'BuiltIn'][getRandomInt(4)],
          pictures,
        };
      }
    ),
  };

  res.status(200).json(newData);
}

function getFilterData(minPrice: number = MIN_PRICE, maxPrice: number = MAX_PRICE, type: string[] = TYPE, availability: string = AVAILABLE, count:number=6) {
  let filteredData = []
  
  for (let d of data.items){
    if (filteredData.length < count){
      if (d.price >= minPrice && d.price <= maxPrice && type.includes(d.vehicleType)){
        if (availability === "yes" && d.instantBookable){
          filteredData.push(d)
        }
        if (availability === "no"){
          filteredData.push(d)
        }
      }
    }
  }
  return filteredData
}