import styled from "styled-components";
import Image from 'next/image'
import logo from '../resources/logo.png'
import Filters from '../src/components/Filters'
import Campers from "../src/components/Campers";
import { server } from "../constants/config";
import axios from "axios";


interface HomeProps {
  data:{
    count: number
    items:[]
  }
}

const Home = (props:HomeProps) => {
  return (
      <PageWrapper>
        <ImageWrapper>
          <Image
            src={logo}
            alt="Logo"
            width={200}
            height={40}
          />
        </ImageWrapper>
        <Filters/>
        <Campers data={props.data.items} count={props.data.count}/>
      </PageWrapper>
  )
}

const PageWrapper = styled.div`

`
const ImageWrapper = styled.div`
  padding:10px;
  padding-left:50px;
  
  @media (  width <= 768px) { 
    display:flex;
    justify-content: space-evenly;
    
    padding-right:5px;
    padding-left:5px;
}
`


export default Home


export async function getServerSideProps() {
  
    const response = await axios.post(`${server}/api/data`, {})

  return {
    props: {
      data:response.data
    }, 
  }
  
}