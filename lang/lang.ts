
import {cs} from "./cs"

export const translate = (language:string,key:string, data:any=null)=>{
    
    const keyArray = key.split(".");
    let lang = cs
    switch (language) {
        case "cs":
                lang = cs
            break;
    
        default:
            break;
    }
    for (let o in lang){
        if (keyArray[0] === o){
            let sentence:any = drillDown(lang[o],1, keyArray)
            if (data !== null){
                for(let d in data){
                    sentence = sentence.replace("!%"+d, data[d]);
                }
            }
            return sentence
        }
    }
}

const drillDown = (root:any,level:number, keyArray:string[]):any => {
    if (level < keyArray.length){
        for (let r in root){
            if (keyArray[level] === r){
                return drillDown(root[r],level+1, keyArray)
            }
        }
    }
    return root
}