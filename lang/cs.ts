export interface Language {
    [key: string]: any;
}

export const cs:Language = {
    general:{
        priceForDay: "Cena za den",
        caravanType: "Typ karavanu",
        availability: "Okamžitá rezervace",
        yes: "Ano",
        no: "Ne",
        priceFrom: "Cena od",
        moreCaravans: "Načíst další",
        noMoreCaravans: "Žádné další dostupné karavany..."
    }
}