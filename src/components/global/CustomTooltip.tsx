import React, { JSXElementConstructor, ReactElement, ReactNode } from 'react'


import Tooltip, { TooltipProps, tooltipClasses } from '@mui/material/Tooltip';
import { styled as styledMui } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';

interface CustomTooltipProps {
  children: ReactElement
  description: string
}


export default function CustomTooltip(props:CustomTooltipProps) {


  const HtmlTooltip = styledMui(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} classes={{ popper: className }} />
  ))(({ theme }) => ({
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: '#f5f5f9',
      color: 'rgba(0, 0, 0, 0.87)',
      maxWidth: 220,
      fontSize: theme.typography.pxToRem(12),
      border: '1px solid #dadde9',
    },
  }));

  return (
    <>
      <HtmlTooltip
        title={
          <React.Fragment>
            <Typography color="inherit">{props.description}</Typography>
          </React.Fragment>
        }
      >
        {props.children}
      </HtmlTooltip>
    </>
  )
}
