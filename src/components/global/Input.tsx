import React, { ChangeEvent, ReactElement } from 'react'
import styled from 'styled-components'


interface InputProps {
    value: number
    type: string
    max?: number
    onChange: (value: any) => void
}

export default function Input(props:InputProps) {

    const handleInputChange = (e:ChangeEvent):void => {
        const element = e.target as HTMLInputElement
        
        if (props.type === 'price' || props.type === 'number'){
            if (Number(element.value) ){
                if (props.max && props.max < Number(element.value)){
                    props.onChange(props.max)
                } else props.onChange(element.value)
            }
            if (element.value === ''){
                props.onChange(element.value)
            }
        }

        
        if (props.type === 'text'){
            if (Number(element.value) ){
                props.onChange(element.value)
            } 
        }

    }

    const getCurrency = (): ReactElement | string => {
        if (props.type === 'price'){
            return (
                <Currency>Kč</Currency>
            )
        }
        return ""
    }

    return (
        <div>
            <InputStyling>
                <UnstyledInput value={props.value} onChange={(e)=>handleInputChange(e)}/>
                {getCurrency()}
            </InputStyling>
        </div>
    )
}

const UnstyledInput = styled.input`

  border: none;
  outline: none;
  width: 90%;
`;

const InputStyling = styled.div`

  border: 1px solid #edebe4;
  border-radius: 5px;
  width: 100%;
  padding:4px;
  display: flex;
`;

const Currency = styled.span`
  color: #aa9c9c;
`;
