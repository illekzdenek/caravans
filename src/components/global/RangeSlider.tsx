import React from "react";
import styled from 'styled-components';
import Slider from '@mui/material/Slider';
import { MAX_PRICE, MIN_PRICE } from "../../../constants/config";

interface RangeSliderProps {
    lower: number;
    higher: number;
    onChange: (value: any) => void;
}

function valuetext(value: number):string {
    return `${value}Kč`;
}

export default function RangeSlider(props: RangeSliderProps) {


    const handleOnChange = (
        e: Event,
        newValue: number | number[]
    ):void => {
        props.onChange(newValue)
    }

    return (
        <Container>
        <Slider
            getAriaLabel={() => 'Minimum distance shift'}
            value={[props.lower,props.higher]}
            onChange={handleOnChange}
            valueLabelDisplay="auto"
            getAriaValueText={valuetext}
            // disableSwap
            min={MIN_PRICE}
            max={MAX_PRICE}
        />
        </Container>
    );
}

const Container = styled.div`

  padding-right:10px;
`;