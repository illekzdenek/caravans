
import React, { BaseSyntheticEvent, ReactElement, useState } from 'react';
import Button from '@mui/material/Button';
import styled from 'styled-components';



interface CustomButtonProps {
  onClick: (event: BaseSyntheticEvent) => void
  children: ReactElement | string
}


export default function CustomButton(props:CustomButtonProps) {


  const handleClick = (e:BaseSyntheticEvent):void => {
    props.onClick(e)
  }


  return (
    <Container>
        <Button  variant="contained" onClick={handleClick}>{props.children}</Button>
    </Container>
    

  )
}

const Container = styled.section`

    margin-top:10px;
    margin-bottom:10px;
    width:100%;
    display:flex;
    
    justify-content: center 
`;