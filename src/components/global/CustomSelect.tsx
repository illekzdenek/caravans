
import React, { ReactElement, useState } from 'react';
// import { Select, Space } from 'antd';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { useWindowWide } from "../../../utils/useWindowWide"
import styled from 'styled-components';



interface CustomSelectProps {
  options: {
    value:string
    label:string
  }[];
  default: string;
  onChange: (event: string) => void
  title: string
}


export default function CustomSelect(props:CustomSelectProps) {

  const wide = useWindowWide(768)
  const [value, setValue] = useState(props.default);

  const handleChange = (event: SelectChangeEvent):void => {
    console.log(event.target.value)
    props.onChange(event.target.value)
    setValue(event.target.value)
  }

  const getOptions = (): ReactElement[] => {
    let optionElements = []
    for (let option of props.options){
      optionElements.push(
        
        <MenuItem value={option.value} key={option.value}>{option.label}</MenuItem>
      )
    }

    return optionElements
  }

  return (
    <Container>
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">{props.title}</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={value}
          label={props.title}
          onChange={handleChange}
          size={wide?"small":"medium"}
        >
          {getOptions()}
        </Select>
      </FormControl>
    </Box>
    </Container>
    

  )
}

const Container = styled.section`

  margin-top:10px;
`;