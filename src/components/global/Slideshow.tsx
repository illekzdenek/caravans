import React, { useEffect, useRef, useState } from "react";
import styled from 'styled-components';
import Image from 'next/image';

interface SlideshowProps {
    images: string[]
}
const delay = 3500;

export default function Slideshow(props: SlideshowProps) {


    const [index, setIndex] = useState(0);
    const [images, setImages] = useState(props.images);
    const timeoutRef = useRef(null);

    useEffect(() => {
        resetTimeout();
        timeoutRef.current = setTimeout(
            () =>
                setIndex((prevIndex) =>
                    prevIndex === images.length - 1 ? 0 : prevIndex + 1
                ),
            delay
        );

        return () => {

            resetTimeout();
        };
    }, [index]);


    function resetTimeout() {
        if (timeoutRef.current) {
            clearTimeout(timeoutRef.current);
        }
    }

    return (
        <Container>
            <div className="slideshow">
                <div className="slideshowSlider"
                    style={{ transform: `translate3d(${-index * 100}%, 0, 0)` }}
                >
                    {images.map((backgroundColor, index) => (
                        <Image src={backgroundColor} className="slide" key={index} width={540} height={288} alt={backgroundColor}/>
                    ))}
                </div>

                <div className="slideshowDots">
                    {images.map((_, idx) => (
                        <div key={idx}

                            className={`slideshowDot${index === idx ? " active" : ""}`}
                            onClick={() => {
                                setIndex(idx);
                            }}
                        ></div>
                    ))}
                </div>
            </div>
        </Container>
    );
}

const Container = styled.div`

//   padding-right:10px;
`;