import React, { ReactElement } from 'react'
import styled from 'styled-components'
import { useAtom } from 'jotai'
import { typeAtom } from '../../../store';



interface TypeCardProps {
    type:string;
    title: string;
    description: string;
    clickCallback: (event:React.MouseEvent<HTMLDivElement, MouseEvent>, type:string) => void
}

export default function TypeCard(props:TypeCardProps) {
    
    const [types, setTypes] = useAtom(typeAtom)

    const getCard = (): ReactElement => {
        if (types.includes(props.type)) {
            return (
                <ContainerSelected onClick={(e)=>props.clickCallback(e, props.type)}>
                    <Title>{props.title}</Title>
                    <Description>{props.description}</Description>
                </ContainerSelected>
            )
        }

        return (
            <Container onClick={(e)=>props.clickCallback(e, props.type)}>
                <Title>{props.title}</Title>
                <Description>{props.description}</Description>
            </Container>
        )
    }

    return (
        <>
            {getCard()}
        </>
    )
}
const Container = styled.div`

    border: 1px solid lightgray;
    padding:4px;
    border-radius:5px;
    width:20%;
    min-width:100px;
    box-sizing:border-box;
    &:hover {
        border: 2px solid green;
        cursor: pointer;
    }
    
    @media (  width <= 400px) { 
        width:40%;
    }
`;

const ContainerSelected = styled.div`

    border: 2px solid green;
    padding:4px;
    border-radius:5px;
    width:20%;
    min-width:100px;
    box-sizing:border-box;
    &:hover {
        cursor: pointer;
    }
    @media (  width <= 400px) { 
        width:40%;
    }
`;

const Title = styled.h4`

    padding:0;
    margin:0;
    font-weight: 400;
    font-size: 16px;
`;

const Description = styled.p`

    padding:0;
    margin:0;
    font-weight: 100;
    font-size: 12px;
`;
