import React from 'react'
import styled from 'styled-components'
import Availability from './Filters/Availability'
import Price from './Filters/Price'
import Type from './Filters/Type'

export default function Filters() {
    return (
        <>
            <div></div>
            <Container>
                <Price/>
                <Type/>
                <Availability/>
            </Container>
        </>
    )
}

const Container = styled.section`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  border-top:1px solid lightgray;
  border-bottom:1px solid lightgray;
  
`;