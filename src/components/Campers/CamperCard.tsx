import React, {ReactElement} from 'react'
import styled from 'styled-components';
import Image from 'next/image';
import Head from 'next/head';
import { translate } from '../../../lang/lang';
import { useAtom } from 'jotai'
import { languageAtom } from '../../../store';


import CustomTooltip from '../global/CustomTooltip';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';


import bed from "../../../resources/bed.png"
import shower from "../../../resources/shower.png"
import toilet from "../../../resources/toilet.png"
import working from "../../../resources/working.png"
import Slideshow from '../global/Slideshow';


interface CamperCardProps {
    data: {
        instantBookable:boolean
        location:string
        name:string
        passengersCapacity:number
        pictures:string[]
        price: number
        shower:boolean
        sleepCapacity:number
        toilet: boolean
        vehicleType:string
    };
}


export default function CamperCard(props:CamperCardProps) {

    
    const [language] = useAtom(languageAtom)

    const getShower = (): ReactElement | string=> {
        if (props.data.shower){
            return <Image src={shower} alt="shower" width={25} height={25}/>
        }

        return ""
    }
    const getToilet = (): ReactElement | string => {
        if (props.data.toilet){
            return <Image src={toilet} alt="toilet" width={25} height={25}/>
        }

        return ""
    }

  return (
        <Container>
            <Head>
                <meta property="og:title" content={props.data.name} key="title"/>
                <meta property="og:description" content={props.data.vehicleType} key="description"  />
                <meta property="og:image" content={props.data.pictures[0]} key="image"/>
            </Head>
            <Slideshow images={props.data.pictures}/>
            <TextContainer>
                <Type>{props.data.vehicleType}</Type>
                <Title>{props.data.name}</Title>
                <Parameters>
                    <Location>{props.data.location}</Location>
                    <Features>
                        <Image src={working} alt="passengersCapacity" width={25} height={25}/> {props.data.passengersCapacity} 
                        <Image src={bed} alt="sleepCapacity" width={25} height={25}/> {props.data.sleepCapacity} 
                        
                        {getToilet()}
                        {getShower()}
                    </Features>
                </Parameters>
                <Price>
                    <div>
                        <PriceLabel>{translate(language, "general.priceFrom")}</PriceLabel>
                    </div>
                    <div>
                        <PriceValue>
                            <TitleContainer>
                                {props.data.price} Kč/den 
                            </TitleContainer>
                            <CustomTooltip description='Možnost slevy při delší výpujční době.'>
                                <HelpOutlineIcon style={{color:"red"}}/>
                            </CustomTooltip>
                        </PriceValue>
                    </div>
                </Price>
            </TextContainer>
        </Container>
  )
}



const Container = styled.section`
    min-width:148px;
    // width:32%;
    border: 1px solid lightgray;
    overflow:hidden;
    border-radius:5px;
    margin-top:25px;
`;
const TextContainer = styled.div`

    padding-left:15px;
    padding-right:15px;
`;
const Title = styled.h1`
`;
const Type = styled.h3`
    color: red;
    font-weight: 400;
    font-size: inherit;
`;
const Parameters = styled.div`
    border-top: 1px solid lightgray;
    border-bottom: 1px solid lightgray;
    padding-top: 10px;
    padding-bottom: 10px;
    display:flex;
    flex-direction: column;
    gap:4px;
`;

const Features = styled.div`
    display:flex;
    gap:4px;
`;
const Location = styled.div`
`;
const Price = styled.div`
    display:flex;
    justify-content: space-between;
    
    padding-top: 10px;
    padding-bottom: 10px;
`;
const PriceLabel = styled.span`
    color:gray;
`;
const PriceValue = styled.span`
    font-weight: 700;
    display: flex;
    align-items: center;
`;


const TitleContainer = styled.span`

  padding:4px;
`;