import React from 'react'
import styled from 'styled-components';
import Input from '../global/Input';
import RangeSlider from '../global/RangeSlider';
import { useAtom } from 'jotai'
import { maxPriceAtom, minPriceAtom, languageAtom } from '../../../store';
import { MAX_PRICE } from '../../../constants/config';
import {translate} from "../../../lang/lang"


export default function Price() {
    
    const [maxPrice, setMaxPrice] = useAtom(maxPriceAtom)
    const [minPrice, setMinPrice] = useAtom(minPriceAtom)
    const [language] = useAtom(languageAtom)

    
    const handleSliderChange = (value:number[]):void => {
        setMaxPrice(value[1])
        setMinPrice(value[0])
    }


    return (
        <Container>
            {translate(language, "general.priceForDay")}
            <RangeSlider lower={minPrice} higher={maxPrice} onChange={(value)=>handleSliderChange(value)}/>
            <InputContainer>
                <Input type="price" max={MAX_PRICE} value={minPrice} onChange={(value)=>setMinPrice(value)}/>
                <Input type="price" max={MAX_PRICE} value={maxPrice} onChange={(value)=>setMaxPrice(value)}/>
            </InputContainer>
        </Container>
    )
}

const Container = styled.section`

  border-right: 1px solid lightgray;
  width: 20%;
  padding:4px;
  padding-left:50px;
  min-width: 148px;
  @media (  width <= 768px) { 
    width:100%;
    padding-left:10px;
    padding-right:10px;
    border-bottom: 1px solid lightgray
  }
`;

const InputContainer = styled.div`
    display:flex;
    gap:4px;
`;