import React from 'react'
import styled from 'styled-components';
import { translate } from '../../../lang/lang';
import { useAtom } from 'jotai'
import { languageAtom, availabilityAtom } from '../../../store';
import CustomSelect from '../global/CustomSelect';
import CustomTooltip from '../global/CustomTooltip';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';




export default function Availability() {
    const [language, setLanguage] = useAtom(languageAtom)
    const [availability, setAvailability] = useAtom(availabilityAtom)


    const options = [
        { value: 'yes', label: translate(language, "general.yes") },
        { value: 'no', label: translate(language, "general.no") },
    ]


    const handleCustomSelectChange = (value: string): void => {
        setAvailability(value)
    }

    return (
        <Container>
            <TitleContainer>
                {translate(language, "general.availability")}
            </TitleContainer>
            <CustomTooltip description='Vybrat pouze ihned dostupné karavany!'>
                <HelpOutlineIcon style={{ color: "red" }} />
            </CustomTooltip>
            <br />
            <CustomSelect title='Dostupné' options={options} default={availability} onChange={handleCustomSelectChange} />
        </Container>
    )
}
const Container = styled.section`

  width: 15%;
  padding:4px;
  padding-right:50px;
  min-width: 80px;
  @media (  width <= 768px) { 
    
    min-width: 148px;
    width:100%;
    padding-right:10px;
    padding-left:10px;
  }
`;
const TitleContainer = styled.span`

  padding:4px;
`;