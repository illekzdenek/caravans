import React from 'react'
import styled from 'styled-components';
import { useAtom } from 'jotai'
import { languageAtom, typeAtom } from '../../../store';
import { translate } from '../../../lang/lang';
import TypeCard from '../global/TypeCard';

export default function Type() {

    
    const [language, setLanguage] = useAtom(languageAtom)
    const [types, setTypes] = useAtom(typeAtom)

    const handleTypeClick = (e:React.MouseEvent<HTMLDivElement, MouseEvent>, selectedType:string):void =>  {

        let newTypesSelected = [...types]
        if (newTypesSelected.includes(selectedType)){
            newTypesSelected.splice(newTypesSelected.indexOf(selectedType),1)
            
        } else {
            newTypesSelected.push(selectedType)
        }
        setTypes(newTypesSelected)
    }
    

    return (
        <Container>
            {translate(language,"general.caravanType")}
            <CardContainer>
                <TypeCard clickCallback={handleTypeClick} type={"Campervan"} title='Campervan' description='Obytka s rozměry osobáku, se kterou dojedete všude.'/>
                <TypeCard clickCallback={handleTypeClick} type={"Intergrated"} title='Integrál' description='Král mezi karavany. Luxus na kolech.'/>
                <TypeCard clickCallback={handleTypeClick} type={"BuiltIn"} title='Vestavba' description='Celý byt geniálně poskládaný do dodávky.'/>
                <TypeCard clickCallback={handleTypeClick} type={"Alcove"} title='Přívěs' description='Tažný karavan za vaše auto. Od kapkovitých až po rodinné.'/>
            </CardContainer>
        </Container>
    )
}
const Container = styled.section`

  border-right: 1px solid lightgray;
  width: 65%;
  padding:4px;
  min-width:412px;
  
  @media (  width <= 768px) { 
    
    min-width: 148px;
    width:100%;
    padding-left:10px;
    padding-right:10px;
    border-bottom: 1px solid lightgray
  }
`;
const CardContainer = styled.div`

  display: flex;
  justify-content: space-evenly;
  gap:4px;
  padding:4px;
  @media (  width <= 768px) { 
    flex-wrap: wrap;
  }
`;