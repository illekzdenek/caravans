import React, { BaseSyntheticEvent, ReactElement, useEffect, useState } from 'react'
import styled from 'styled-components';
import CamperCard from './Campers/CamperCard'
import { useAtom } from 'jotai'
import { availabilityAtom, languageAtom, maxPriceAtom, minPriceAtom, typeAtom } from '../../store';
import axios from 'axios';
import { server } from '../../constants/config';
import CustomButton from './global/CustomButton';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import { translate } from '../../lang/lang';


interface CampersProps {
    count: number
    data: {
        instantBookable: boolean
        location: string
        name: string
        passengersCapacity: number
        pictures: string[]
        price: number
        shower: boolean
        sleepCapacity: number
        toilet: boolean
        vehicleType: string
    }[];
}

export default function Campers(props: CampersProps) {

    const [language, setLanguage] = useAtom(languageAtom)
    const [minPrice, setMinPrice] = useAtom(minPriceAtom)
    const [maxPrice, setMaxPrice] = useAtom(maxPriceAtom)
    const [type, setType] = useAtom(typeAtom)
    const [availability, setAvailability] = useAtom(availabilityAtom)

    const [open, setOpen] = useState(true);
    const [campers, setCampers] = useState(props.data)
    const [count, setCount] = useState(6)
    const [countAvailable, setCountAvailable] = useState(count)

    useEffect((): void => {

        handleToggle()
        axios.post(`${server}/api/data`, {
            minPrice,
            maxPrice,
            type,
            availability,
            count
        }).then((res) => {
            setCampers(res.data.items)
            setCountAvailable(res.data.count)
            handleClose()
        }).catch((err) => {
            console.log(err)
            handleClose()
        })

    }, [minPrice, maxPrice, type, availability, count])

    const handleClose = () => {
        setOpen(false);
    };
    const handleToggle = () => {
        setOpen(!open);
    };

    const handleOnClickNextCaravans = (e: BaseSyntheticEvent): void => {
        setCount(count + 6)
    }

    const getCampersCards = (): ReactElement => {
        let campersElements = []
        let counter = 0
        for (let camper of campers) {
            campersElements.push(
                <CamperCard data={camper} key={camper.name + camper.location + counter} />
            )
            counter += 1
        }

        return (
            <>
                {campersElements}
            </>
        )
    }

    const getNextCaravansButton = (): ReactElement | string => {
        if (count > countAvailable) {
            return (
                <NoMoreCaravans>
                    {translate(language, "general.noMoreCaravans")}
                </NoMoreCaravans>
            )
        }

        return (
            <>
                <CustomButton onClick={handleOnClickNextCaravans}>{translate(language, "general.moreCaravans")}</CustomButton>

            </>
        )
    }

    return (
        <div>
            <>
            <Backdrop
                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open={open}
            >
                <CircularProgress color="inherit" />
            </Backdrop>
                <Container>
                    {getCampersCards()}

                </Container>
                {getNextCaravansButton()}
            </>
        </div>
    )
}


const Container = styled.section`
    display:flex;
    gap:10px;
    width:100%;
    flex-wrap: wrap;
    // justify-content: space-between;
    justify-content: flex-start
    
    
    padding-right:50px;
    padding-left:50px;
    padding-top: 25px;
    
    @media (  width <= 1277px) { 
        
        justify-content: space-evenly;
        
        padding-right:5px;
        padding-left:5px;
    }
   
`;
const NoMoreCaravans = styled.div`
    width:100%;
    display:flex;
    justify-content: center;
    margin-top:10px;
    margin-bottom:10px;
   
`;
