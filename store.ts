import { atom } from "jotai"
import { AVAILABLE, MAX_PRICE, MIN_PRICE, TYPE } from "./constants/config"


export const maxPriceAtom = atom<number>(MAX_PRICE)
export const minPriceAtom = atom<number>(MIN_PRICE)
export const languageAtom = atom<string>("cs")
export const typeAtom = atom<string[]>(TYPE)
export const availabilityAtom = atom<string>(AVAILABLE)